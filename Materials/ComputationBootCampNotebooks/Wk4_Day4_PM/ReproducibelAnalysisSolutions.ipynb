{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reproducible Analysis\n",
    "\n",
    "![Jelly beans](https://imgs.xkcd.com/comics/significant.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## 10 Rules for Reproducible Analysis\n",
    "\n",
    "From [Ten Simple Rules for Reproducible Computational Research](http://journals.plos.org/ploscompbiol/article?id=10.1371/journal.pcbi.1003285)\n",
    "\n",
    "- Rule 1: For Every Result, Keep Track of How It Was Produced\n",
    "- Rule 2: Avoid Manual Data Manipulation Steps\n",
    "- Rule 3: Archive the Exact Versions of All External Programs Used\n",
    "- Rule 4: Version Control All Custom Scripts\n",
    "- Rule 5: Record All Intermediate Results, When Possible in Standardized Formats\n",
    "- Rule 6: For Analyses That Include Randomness, Note Underlying Random Seeds\n",
    "- Rule 7: Always Store Raw Data behind Plots\n",
    "- Rule 8: Generate Hierarchical Analysis Output, Allowing Layers of Increasing Detail to Be Inspected\n",
    "- Rule 9: Connect Textual Statements to Underlying Results\n",
    "- Rule 10: Provide Public Access to Scripts, Runs, and Results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### For Every Result, Keep Track of How It Was Produced\n",
    "\n",
    "- This is quite natural if you use a Jupyter notebook\n",
    "- Use Markdown cells to document how each result is produced"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Avoid Manual Data Manipulation Steps\n",
    "\n",
    "- Copy and paste is dangerous\n",
    "- GUI operations are not reproducible\n",
    "- Use a script\n",
    "- An entire notebook can be used as a script\n",
    "    - `Cell | Run All`\n",
    "- You can generate HTML or PDF pages from a notebook automatically with [nbconvert](https://nbconvert.readthedocs.io)\n",
    "```bash\n",
    "jupyter nbconvert --to FORMAT notebook.ipynb\n",
    "```\n",
    "where ```FORMAT``` can be `pdf`, `html` etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Archive the Exact Versions of All External Programs Used\n",
    "\n",
    "- This is becoming easier with virtual environments and Docker micro-containers\n",
    "- At least, record what versions of every package you are using"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "\n",
      "Attaching package: ‘dplyr’\n",
      "\n",
      "The following objects are masked from ‘package:stats’:\n",
      "\n",
      "    filter, lag\n",
      "\n",
      "The following objects are masked from ‘package:base’:\n",
      "\n",
      "    intersect, setdiff, setequal, union\n",
      "\n"
     ]
    }
   ],
   "source": [
    "library(tidyr)\n",
    "library(dplyr)\n",
    "library(ggplot2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "R version 3.3.0 (2016-05-03)\n",
       "Platform: x86_64-apple-darwin13.4.0 (64-bit)\n",
       "Running under: OS X 10.11.5 (El Capitan)\n",
       "\n",
       "locale:\n",
       "[1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8\n",
       "\n",
       "attached base packages:\n",
       "[1] stats     graphics  grDevices utils     datasets  methods   base     \n",
       "\n",
       "other attached packages:\n",
       "[1] ggplot2_2.1.0 dplyr_0.5.0   tidyr_0.5.1  \n",
       "\n",
       "loaded via a namespace (and not attached):\n",
       " [1] Rcpp_0.12.5          digest_0.6.9         assertthat_0.1      \n",
       " [4] IRdisplay_0.3.0.9000 plyr_1.8.4           grid_3.3.0          \n",
       " [7] repr_0.7             R6_2.1.2             gtable_0.2.0        \n",
       "[10] DBI_0.4-1            jsonlite_1.0         magrittr_1.5        \n",
       "[13] scales_0.4.0         evaluate_0.9         stringi_1.1.1       \n",
       "[16] uuid_0.1-2           IRkernel_0.6         tools_3.3.0         \n",
       "[19] stringr_1.0.0        munsell_0.4.3        colorspace_1.2-6    \n",
       "[22] base64enc_0.1-3      pbdZMQ_0.2-3         tibble_1.1          "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "sessionInfo()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1] \"tidyr 0.5.1\"\n",
      "[1] \"dplyr 0.5.0\"\n",
      "[1] \"ggplot2 2.1.0\"\n"
     ]
    }
   ],
   "source": [
    "for (package in c(\"tidyr\", \"dplyr\", \"ggplot2\")) {\n",
    "    print(paste(package, packageVersion(package)))\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Version Control All Custom Scripts\n",
    "\n",
    "- Continue to learn how to use `git`\n",
    "- If you push regularly to a remote repository, it serves as a backup system\n",
    "- Whenever a change is committed, it is logged and you can revert to an earlier version\n",
    "\n",
    "```bash\n",
    "git log --stat | less\n",
    "\n",
    "commit d36d1b04bcf7f1f5f740e966bc98af9da3d1cdab\n",
    "Merge: 7ffc34f b6890d9\n",
    "Author: Bitnami <bitnami@ubuntu14-generic-template-01.oit.duke.edu>\n",
    "Date:   Wed Jul 27 16:26:21 2016 -0400\n",
    "\n",
    "    Merge branch 'master' of gitlab.oit.duke.edu:Biostatistics_and_Bioinformatic\n",
    "s/HTS_Summer_Course_2016\n",
    "\n",
    "commit 7ffc34f0f5d531663191d21fa7aad14643d19cd9\n",
    "Author: Bitnami <bitnami@ubuntu14-generic-template-01.oit.duke.edu>\n",
    "Date:   Wed Jul 27 16:25:38 2016 -0400\n",
    "\n",
    "    Updated install_Rpackages.ipynb and check_Rpackages.ipynb with packages for Kouros.\n",
    "\n",
    " setup_nb/check_Rpackages.ipynb   | 81 +++++++++++++++++++++++++++++++++++++++-\n",
    " setup_nb/install_Rpackages.ipynb | 48 +++++++++++++++++++++++-\n",
    " 2 files changed, 125 insertions(+), 4 deletions(-)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Record All Intermediate Results, When Possible in Standardized Formats\n",
    "\n",
    "- If it takes a long time to generate intermediate results, it makes sense to store them\n",
    "- For storage, use standard formats, preferably text (e.g. CSV)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### For Analyses That Include Randomness, Note Underlying Random Seeds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<ol class=list-inline>\n",
       "\t<li>-0.560475646552213</li>\n",
       "\t<li>-0.23017748948328</li>\n",
       "\t<li>1.55870831414912</li>\n",
       "\t<li>0.070508391424576</li>\n",
       "\t<li>0.129287735160946</li>\n",
       "\t<li>1.71506498688328</li>\n",
       "\t<li>0.460916205989202</li>\n",
       "\t<li>-1.26506123460653</li>\n",
       "\t<li>-0.686852851893526</li>\n",
       "\t<li>-0.445661970099958</li>\n",
       "</ol>\n"
      ],
      "text/latex": [
       "\\begin{enumerate*}\n",
       "\\item -0.560475646552213\n",
       "\\item -0.23017748948328\n",
       "\\item 1.55870831414912\n",
       "\\item 0.070508391424576\n",
       "\\item 0.129287735160946\n",
       "\\item 1.71506498688328\n",
       "\\item 0.460916205989202\n",
       "\\item -1.26506123460653\n",
       "\\item -0.686852851893526\n",
       "\\item -0.445661970099958\n",
       "\\end{enumerate*}\n"
      ],
      "text/markdown": [
       "1. -0.560475646552213\n",
       "2. -0.23017748948328\n",
       "3. 1.55870831414912\n",
       "4. 0.070508391424576\n",
       "5. 0.129287735160946\n",
       "6. 1.71506498688328\n",
       "7. 0.460916205989202\n",
       "8. -1.26506123460653\n",
       "9. -0.686852851893526\n",
       "10. -0.445661970099958\n",
       "\n",
       "\n"
      ],
      "text/plain": [
       " [1] -0.56047565 -0.23017749  1.55870831  0.07050839  0.12928774  1.71506499\n",
       " [7]  0.46091621 -1.26506123 -0.68685285 -0.44566197"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "set.seed(123)\n",
    "rnorm(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Always Store Raw Data behind Plots\n",
    "\n",
    "Even better, provide a script that generates each plot automatically without any manual manipulation at all. We have seen several examples of generating publication quality graphics starting from CSV files using `ggplot`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generate Hierarchical Analysis Output, Allowing Layers of Increasing Detail to Be Inspected\n",
    "\n",
    "For example, provide a hyperlink to the original data and script in the figure caption, so that it is easy for other researches to understand how your results were generated and to replicate your results. Generally, if your figures and tables and summary statistics are documented and generated via a Jupyter notebook, it will be easy for others to re-analyze your work."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Connect Textual Statements to Underlying Results\n",
    "\n",
    "Use the Jupyter notebook or other literate programming tools"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Provide Public Access to Scripts, Runs, and Results\n",
    "\n",
    "When you publish a paper, make the data and scripts (e.g. Jupyter notebook) available, either as Supplementary Materials or by providing a link to a publicly available repository."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 10 Personal Rules\n",
    "\n",
    "0. Have a data analysis plan **before** you do the experiment.\n",
    "0. Stick to the plan. Avoid data trawling.\n",
    "0. With genomic data, be especially watchful for **noise discovery**.\n",
    "0. Simulations are often a good sanity check.\n",
    "1. Do not ever edit raw data.\n",
    "2. Use a scripted and reproducible pipeline from raw data to final results. Avoid manual data manipulation.\n",
    "3. Keep good notes together with your code so that someone else (or yourself) can understand the analysis. One way to do this is with liberal use of Markdown cells in a Jupyter notebook, or by using `knitr` and Rmarkdown in RStudio.\n",
    "4. Keep track of all changes to your code by using version control.\n",
    "5. Keep track of all software used to generate your results, including version numbers. This can be done by using a virtual environment or Docker container.\n",
    "6. Make your data and scripts publicly available on publication so others can replicate your results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.3.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
