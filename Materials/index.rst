.. Duke HTS 2016.

Duke HTS 2016
==============================================================

`Full contents for each topic are in the GitLab repository <http://gitlab.oit.duke.edu/Biostatistics_and_Bioinformatics/HTS_Summer_Course_2016.git>`_

Lecture notes
----

Statistics
^^^^^^^^^^^^^^^^^^^^^^^

- :download:`Experimental Design Part 1 <Statistics/07062016/ExpDesign.part_I.pdf>`
- :download:`Experimental Design Part 2 <Statistics/07062016/ExpDesign_PartII.pdf>`
- :download:`Statistical Inference Handout <Statistics/07182016/stat-inference-handout.pdf>`
- :download:`Statistical Inference <Statistics/07182016/stat-inference.pdf>`
- :download:`Introduction to Statistics Handout <Statistics/07182016/stat-introduction-handout.pdf>`
- :download:`Introduction to Statistics <Statistics/07182016/stat-introduction.pdf>`
- :download:`Statistical Estimation Handout <Statistics/07192016/stat-estimation-handout.pdf>`
- :download:`Statistical Estimation <Statistics/07192016/stat-estimation.pdf>`
- :download:`Sources of Variability Handout <Statistics/07192016/stat-source-of-variability-handout.pdf>`
- :download:`Sources of Variability <Statistics/07192016/stat-source-of-variability.pdf>`
- :download:`Unsupervised Learning Handout <Statistics/07212016/stat-supervised-handout.pdf>`
- :download:`Unsupervised Learning <Statistics/07202016/stat-unsupervised.pdf>`
- :download:`Supervised Learning Handout <Statistics/07212016/stat-supervised-handout.pdf>`
- :download:`Supervised Learning <Statistics/07212016/stat-supervised.pdf>`
- :download:`Multiple Testing Handout <Statistics/07252016/stat-multipletesting-handout.pdf>`
- :download:`Multiple Testing <Statistics/07252016/stat-multipletesting.pdf>`
- :download:`GLM for RNA-Seq Handout <Statistics/07262016/stat-GLM-model-RNA-Seq-handout.pdf>`
- :download:`GLM for RNA-Seq <Statistics/07262016/stat-GLM-model-RNA-Seq.pdf>`


Bioinformatics
^^^^^^^^^^^^^^^^^^^^^^^

- :download:`Data Standards <Bioinformatics/Data_Standards.pdf>`
- :download:`Standards Tools and Resources <Bioinformatics/Standards_Tools_Resources.pdf>`
- :download:`clinical Terminologies <Bioinformatics/Clinical_Terminologies.pdf>`
- :download:`Ontologies <Bioinformatics/NGS_ontology_class_2016.pdf>`


Jupyter notebooks
--------

R programming
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   ComputationBootCampNotebooks/Wk1_Day1_PM/IntroToCompBoot.ipynb
   ComputationBootCampNotebooks/Wk1_Day1_PM/BasicRinJupyter.ipynb
   ComputationBootCampNotebooks/Wk1_Day1_PM/IntroductionToR.ipynb
   ComputationBootCampNotebooks/Wk1_Day2_PM/More Basic R and Writing custom functions in R.ipynb
   .. ComputationBootCampNotebooks/Wk1_Day3_PM/More Basic R and Writing custom functions in R-Continued.ipynb
   ComputationBootCampNotebooks/Wk1_Day3_PM/Unix Shell-Solutions.ipynb
   .. ComputationBootCampNotebooks/Wk1_Day3_PM/Unix Shell.ipynb
   .. ComputationBootCampNotebooks/Wk2_Day4_PM/CleaningAndPreparingData.ipynb
   ComputationBootCampNotebooks/Wk2_Day4_PM/CleaningAndPreparingDataSolutions.ipynb
   ComputationBootCampNotebooks/Wk3_Day1_PM/ManipulatingData-Solutions.ipynb
   .. ComputationBootCampNotebooks/Wk3_Day1_PM/ManipulatingData.ipynb
   ComputationBootCampNotebooks/Wk3_Day1_PM/VisualizingData.ipynb
   .. ComputationBootCampNotebooks/Wk3_Day1_PM/VisualizingData-Solutions.ipynb
   ComputationBootCampNotebooks/Wk3_Day2_PM/BasicStatistics-Solutions.ipynb
   .. ComputationBootCampNotebooks/Wk3_Day2_PM/BasicStatistics.ipynb
   .. ComputationBootCampNotebooks/Wk3_Day2_PM/HypothesisTesting.ipynb
   ComputationBootCampNotebooks/Wk3_Day2_PM/HypothesisTestingSolutions.ipynb
   ComputationBootCampNotebooks/Wk3_Day2_PM/PowerAndSampleSizeSolutions.ipynb
   ComputationBootCampNotebooks/Wk3_Day2_PM/WorkingWithProbabiltyDistributions-Solutions.ipynb
   .. ComputationBootCampNotebooks/Wk3_Day2_PM/WorkingWithProbabiltyDistributions.ipynb
   ComputationBootCampNotebooks/Wk3_Day3_PM/UnsupervisedLearningAndR.ipynb
   ComputationBootCampNotebooks/Wk3_Day4_PM/UnsupervisedLearningAndGeneExpression.ipynb
   .. ComputationBootCampNotebooks/Wk3_Day4_PM/RFormulasAndStatisticalModeling.ipynb
   ComputationBootCampNotebooks/Wk3_Day4_PM/SupervisedLearning.ipynb
   ComputationBootCampNotebooks/Wk3_Day4_PM/SupervisedLearningWhatCouldGoWrong.ipynb
   .. ComputationBootCampNotebooks/Wk4_Day1_PM/CountingMethods.ipynb
   ComputationBootCampNotebooks/Wk4_Day1_PM/CountingMethodsSolutions.ipynb
   ComputationBootCampNotebooks/Wk4_Day1_PM/RFormulaAndRegressionSolutions.ipynb
   ComputationBootCampNotebooks/Wk4_Day2_PM/CountingMethodsSolutions.ipynb
   ComputationBootCampNotebooks/Wk4_Day2_PM/MutlipleTestingSolutions.ipynb
   ComputationBootCampNotebooks/Wk4_Day3_AM/DESeq2-Notebook.ipynb
   ComputationBootCampNotebooks/Wk4_Day3_PM/GLMSolutions.ipynb
   .. ComputationBootCampNotebooks/Wk4_Day3_PM/ReviewExercises.ipynb
   ComputationBootCampNotebooks/Wk4_Day4_PM/ReproducibelAnalysisSolutions.ipynb
   ComputationBootCampNotebooks/Wk4_Day4_PM/FinalNotes.ipynb
   ComputationBootCampNotebooks/Wk4_Day3_PM/Review.ipynb
   ComputationBootCampNotebooks/Wk4_Day4_PM/FinalExamForYourApprenticeLicense-Solutions.ipynb
   .. ComputationBootCampNotebooks/Wk5_Day3/check_md5sum.ipynb
   .. ComputationBootCampNotebooks/Wk5_Day3/check_read_counts.ipynb

Bioinformatics
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 2

   ComputationBootCampNotebooks/bioconductor-pkg-installation.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinformatics_toc.ipynb
   ComputationBootCampNotebooks/Wk4_Day3_AM/DESeq2-Notebook.ipynb
   ComputationBootCampNotebooks/Wk4_Day3_AM/DESeq2-Notebook-Advanced.ipynb
   ComputationBootCampNotebooks/Wk4_Day4_AM/quality_scores.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/check_read_counts.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/prep_nb/maintenance.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_clean_nb/jupyter_tutorial_clean.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_clean_nb/bash_tutorial_clean.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/demultiplex.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/fastq_intro.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/fastqc.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/fastq_trimming.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/mapping.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/counting.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/paired_pilot_data_pipeline.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/loop_pipeline.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/full_pilot_data_pipeline.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/quality_scores.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/bash_magic.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/file_transfer_and_visualization.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/putting_it_together.ipynb
   .. ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/demux_exploration.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/annotation_prep.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/putting_it_together_all_data.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/working_with_your_data.ipynb
   .. ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/annotation.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/DESeq2-Notebook.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/DESeq2-Notebook-from-Matrix.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/download_through_jupyter.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/update_notebooks.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/2016_full_data_pipeline.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/concatenate_2016_data.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/cyberduck_download.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/download_mrsa_genome.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/full_2015_data_demux.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/full_2015_data_pipeline.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/full_2015_data_qc.ipynb
   ComputationBootCampNotebooks/Wk5_Day3/bioinf_nb/igv_visualization.ipynb

   ComputationBootCampNotebooks/Wk6_Day1_PM/ecoli-bioconductor.ipynb
   ComputationBootCampNotebooks/Wk6-Day3_AM/DESeq2-Notebook-template-Solutions.ipynb

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
