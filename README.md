
# SSH
You will need to SSH into your VM for several of the steps below!

# Git Pull
First of all, do a "git pull". In your SSH session run the following in your ssh session:
1. `cd ~/HTS_Summer_Course_2016/`
2. `git stash`
3. `git pull origin master`

# Check Share and Scratch
Next we need to check to be sure that the "share" and "scratch" directories are available
## Check Share
The following cell should output:

`2015_data
2016
2016_data`

If you don't see that, you probably need to remount the "share" directory.  See instructions below


```python
%%bash
ls -1 ~/work/share
```

## Check Scratch
The following cell should output:
`/home/jovyan/work/scratch/test`

If you don't see that, you probably need to remount the "scratch" directory.  See instructions below


```python
%%bash
touch ~/work/scratch/test
ls ~/work/scratch/test
# cleanup
rm ~/work/scratch/test
```

## Mount Share and Scratch
If checking share or scratch indicated that they are not mounted, you will need to run the following commands in an SSH session:

`bash ~/mount_cmd`

You will need to enter your bitnami password, then your NetID password.

Now, in your ssh session try:
1. `ls ~/share`, which should output: `2015_data  2016  2016_data`
2. `touch ~/scratch/test`
3. `ls ~/scratch/test` which should output: `/home/bitnami/scratch/test`

# Restart Docker
Anytime you have to remount the share and scratch directories, docker needs to be restarted to recognize them. In our case, we need to do a "docker pull" anyway, so that will take care of things.  But first lets clean up.

## Clean Up Docker
In your ssh session run the following command to remove old images and containers:

`~/HTS_Summer_Course_2016/vm_scripts/clean_docker`

## Start Docker
Now we need to pull an updated docker image and start it using the following command in our ssh session:

`~/HTS_Summer_Course_2016/vm_scripts/start_docker`

## Open Jupyter in Chrome
Enough said!
